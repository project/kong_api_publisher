## CONTENTS OF THIS FILE

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## INTRODUCTION

- The Kong API Publisher import the OAS to kong entities likes services and routes.
- The module is supporting both OAS2 and OAS3.

# How it works?

Kong API Publisher modules allows developers to import OAS(Open API Spec) to Kong API Gateway.
![kong_api_publisher](./images/kong_api_publisher.png)

## Requirements

- PHP 7 and above

## Installation

- Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/895232/ for further information.

## Configuration

### Kong Configuration

- /admin/config/kong-api-publisher/config
- enter kong Admin URL

### OAS2 import

- /admin/config/kong-api-publisher/import-oas
- Import OAS2 or OAS3 either in yml or josn format.

### Limitations

- After importing the API specification we can not delete it.
- Currently two policies are supported by the module

## Maintainers

Current maintainers:

- Dileep Maurya(dileepmaurya) - https://www.drupal.org/u/dileepmaurya
